const express = require("express");
const connection = require("./models").connection;
const router = require("./routes");
const app = express();

app.use(express.json());

let port = 8081;

app.use("/api", router);

app.get("/reset", (req, res) => {
  connection
    .sync({ force: true })
    .then(() => {
      res.status(201).send({ message: "Database reset" });
    })
    .catch(() => {
      res.status(500).send({ message: "Database reset failed" });
    });
});

app.use("/*", (req, res) => {
  res.status(200).send("App ruleaza");
});

app.listen(port, () => {
  console.log("Server is running on " + port);
});

app.post('/insule', (req,res) =>
{
  const insula={
    nume: req.body.nume,
    suprafata: req.body.suprafata

  }
  let errors= [];
  if(!insula.nume || !insula.suprafata){
    errors.push("Nu ai completat toate campurile")

  }
  if(errors.length == 0)
  {
    try {const insertQuerry =`INSERT INTO insula(nume, suprafata) VALUES ('${insula.nume}', '${insula.suprafata}')`
    connection.querry(insertQuerry, err =>{
      if(err) throw err;
      else {
        console.log("Insula inserata!")
        res.status(200).send({message: "Insula inserata!"})
      }
    })
    }catch (err){
       console.log("Server error!")
       req.status(500).send(err);
    }
  }

})
app.delete("/crocodili/:id", (req, res) => {
  try {
    const stergere = `DELETE FROM Crocodili WHERE id = '${req.params.id}'`;
    connection.query(stergere, (err) => {
      if (err) throw err;
      res.status(200).send({ message: "Crocodil sters!" });
    });
  } catch {
    console.log("Server error");
    res.status(500).send(err);
  }
});
app.update("/crocodili/:id", (req, res) => {
  try {
    const nume = `UPDATE Crocodili SET nume='${req.crocodil.nume}'`;
    connection.query(sqlDelete, (err) => {
      if (err) throw err;
      res.status(200).send({ message: "Crocodil schimbat" });
    });
  } catch {
    console.log("Server error");
    res.status(500).send(err);
  }
});
